# Functions and their syntax

## Pattern Matching

## Guards

* A bit like an if statement. Returns the first thing it matches e.g.
```haskell
guardFunctionExample :: (Num a) => a -> String
guardFunctionExample x
  | x <= 10   = "You entered ten or less."
  | x <= 20   = "You entered twenty or less."
  | x <= 30   = "You entered thirty or less."
  | otherwise = "You entered more than thirty."
```