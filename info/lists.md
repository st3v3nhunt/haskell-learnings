# Lists

* Lists are homogenous i.e. a list can not contain multiple types
* A list can contain a list of lists. All lists must be of the same type
* Strings are lists of Chars e.g. `[Char]`
  * A string is syntactic sugar for a list of Char. `"hello" == ['h','e','l','l','o']`
* List concatenation achieved by the `++` operator e.g.
  * `[1,2,3] ++ [4,5,6] == [1,2,3,4,5,6]`
* Adding an item to the beginning of the list is achieved by the `:` operator. The cons operator. e.g.
  * `0:[1,2,3] == [0,1,2,3]`
* `++` takes 2 lists. `:` takes a single item and list. Checkout the signature of the functions
  * `:t (++) -- (++) :: [a] -> [a] -> [a]`
  * `:t (:) -- (:) :: a -> [a] -> [a]`
* Using square brackets around elements is syntactic sugar e.g.
  * `[1,2,3] == 1:2:3:[]`
* Taking an item from a list can be achieved by the `!!` operator, using a 0 based index e.g.
  * `"hello" !! 1 == 'e'`
  * `[0,1,2,3,4] !! 4 == 4`
* Lists can be compared if they contain a Type that is comparable. The head element is compared then the second, etc. e.g.
  * `[1] > [0] -- returns True`
  * `[1] > [0,1] -- returns True`
  * `[1] >= [0,1] -- returns True`
  * `[1] >= [1,0] -- returns False`


## Functions for lists

* `head` - the first element e.g.
  * `head [0,1,2,3,4,5,6,7,8,9] -- returns 0`
* `tail` - everything but the head e.g.
  * `tail [0,1,2,3,4,5,6,7,8,9] -- returns [1,2,3,4,5,6,7,8,9]`
* `last` - the last element e.g.
  * `tail [0,1,2,3,4,5,6,7,8,9] -- returns 9`
* `init` - everything but the tail e.g.
  * `init [0,1,2,3,4,5,6,7,8,9] -- returns [0,1,2,3,4,5,6,7,8]`
* `length` - the length of the list e.g. 
  * `length [0,1,2,3,4,5,6,7,8,9]  -- returns 10`
* `null` - boolean indicating if a list is empty e.g. 
  * `null [0] -- returns False`
  * `null []  -- returns True`
* `reverse` - reverses a list e.g.
  * `reverse [0,1,2,3,4,5,6,7,8,9] -- returns [9,8,7,6,5,4,3,2,1,0]`
* `take` - takes the number of elements from the list requested. no error when asking for more elements than there are in the list e.g.
  * `take 2 [0,1,2,3,4,5,6,7,8,9] -- returns [0,1]`
  * `take 10 [1,2] -- returns [1,2]`
  * `take 0 [0,1,2,3,4,5,6,7,8,9] -- returns []`
* `drop` - drops (or removes) the requested number of elements from the list e.g.
  * `drop 5 [0,1,2,3,4,5,6,7,8,9] -- returns [5,6,7,8,9]`
  * `drop 20 [0,1,2,3,4,5,6,7,8,9] -- returns []`
* `maximum` - returns the maximum element from the list. exception for empty list e.g.
  * `maximum [0,1,2,3,4,5,6,7,8,9] -- returns 9`
* `minimum` - returns the minimum element from the list. exception for empty list e.g.
  * `minimum [0,1,2,3,4,5,6,7,8,9] -- returns 0`
* `sum` - sums the contents of the list e.g.
  * `sum [0,1,2,3,4,5,6,7,8,9] -- returns 45`
  * `sum [] -- returns 0`
* `product` - multiplies all elements in the list e.g.
  * `product [0,1,2,3,4,5,6,7,8,9] -- returns 0 (0 in the list)`
  * `product (tail [0,1,2,3,4,5,6,7,8,9]) -- returns 362880`
* `elem` - returns a boolean value indicating whether the element is in the list. usually used as in infix function e.g. 
  * ``5 `elem` [0,1,2,3,4,5,6,7,8,9] -- returns True``
  * ``15 `elem` [0,1,2,3,4,5,6,7,8,9] -- returns False``
* `zip` - zips 2 lists together to produce a list of pairs e.g.
  * `zip [1..8]['a'..'h'] -- returns [(1,'a'),(2,'b'),(3,'c'),(4,'d'),(5,'e'),(6,'f'),(7,'g'),(8,'h')]`
  * if the lists are not equal in size zip still works and will return the number of elements in the smallest list. this means infinite and finite lists can be combined e.g.
  * `zip [1..]["hello","cruel","world"] -- returns [(1,"hello"),(2,"cruel"),(3,"world")]`


## Ranges

* Enumerable Types can be written as ranges e.g.
  * `[1..10] == [0,1,2,3,4,5,6,7,8,9] -- returns True`
  * `[1..10] -- returns [0,1,2,3,4,5,6,7,8,9]`
  * `['a'..'z'] -- returns "abcdefghijklmnopqrstuvwxyz"`
  * `['A'..'Z'] -- returns "ABCDEFGHIJKLMNOPQRTUVWXYZ"`
* Ranges can be specificed with a step. this is done by specifying the first two elements comma separated then the upper limit e.g.
  * `[2,4..10] -- returns [2,4,6,8,10]`
  * `[2,5..10] -- returns [2,5,8]`
* Counting backwards is possible with ranges and steps e.g.
  * `[10..1] -- returns []`
  * `[10,9..1] -- returns [10,9,8,7,6,5,4,3,2,1]`
* No limit needs to be applied to a range, this is a way to create an infinite list e.g.
  * `[1..] -- returns [1,2,3,4..] (continues forever)`
  * `take 99 [21,42..] -- returns the first 99 multiples of 21`


## Infinite list functions

* `cycle` - cycles the list infinitely so requires some other function to be made useful and make it end e.g.
  * `take 9 cycle [1,2,3] -- returns [1,2,3,1,2,3,1,2,3]`
* `repeat` - repeats an element infinitely e.g.
  * `repeat "hello" -- returns ["hello","hello"..] (forever)`
* `replicate` - replicates the element n times e.g.
  * `replicate 3 "abc" -- returns ["abc","abc","abc"]`


## List comprehensions

* A list comprehension looks like `[x*2 | x <- [1..10]]` where `x` is the variable, `[1..10]` is the input, `x*2` is the output function.
  * `[x*2 | x <- [1..10]] -- returns [2,4,6,8,10,12,14,16,18,20]`
* A predicate can be added to the comprehension. Predicates are comma separated and specified after the binding parts
* Using the list comprehension with a predicate to return only those elements greater than or equal to 12 e.g.
  * `[x*2 | x <- [1..10], x*2 >= 12] -- returns [12,14,16,18,20] or elements from the list [1..10] that when multipled by 2 are greater than or equal to 12`
  * `[x | x <- [1..30], x `mod` 3 == 0]  -- returns [3,6,9,12,15,18,21,24,27,30] or elements in the list 1..30 that are exactly divisible by 3`
* This type of predicate matching is also called filtering
* The output function can be any function, here creating a function for later use e.g.
  * ``let fizzBuzz xs = [if x `mod` 3 == 0 then "fizz" else "buzz" | x <- xs]``
* Multiple predicates can be specified e.g.
  * `[x*2 | x <- [1..10], x/=2, x/=3, x/=6] -- returns [2,8,10,14,16,18,20]`
* Multiple lists can be specified e.g.
  * `[x*y | x <- [1,2,3], y <- [1,2,3]] -- returns [1,2,3,2,4,6,3,6,9]`
* Predicates can be specified with multiple inputs e.g.
  * `[x*y | x <- [1..10], y <- [1..10], (x*y > 10) && (x*y < 20)] -- returns all products of x and y between 11 and 19`
* Rewriting the length function `let length' xs = sum [1 | _ <- xs]`
  * every element from the list is taken using `_`
  * 1 is used for every element
  * sum is used to sum all 1s
  * `length' ['a'..'z'] == 26`

