# Types and Typeclasses

* Unless creating a very short function it is good form to provide a type declaration of the function e.g.
  * `functionTakingAStringAndReturningAString :: [Char] -> [Char]`. the function declaration can also be written as:
  * `functionTakingAStringAndReturningAString :: String -> String`. the actual function follows:
  * `functionTakingAStringAndReturningAString x = x`. the function just returns what is passed in
* `::` is the 'has type' operator
* Multiple parameter functions can be declared like so. The last pararmeter is the return type
  * `multipleParameterFunction :: String -> String -> String -> Int`
  * `multipleParameterFunction x y z = length x + length y + length z` -- returns the length of the 3 strings passed in
* Functions can have type variables which are normally represented with the letters a,b,c,d, etc. e.g.
  * `:t head -- returns head :: [a] -> a`
  * `:t fst -- returns fst :: (a, b) -> a`


## Typeclasses

* Typeclasses are a bit like interfaces in that they require the types to implement certain functions
* `Eq` - types that can be tested for equality
* `Eq` typeclasses implement `==` and `/=` e.g.
  * `:t (==) -- returns (==) :: (Eq a) => a -> a -> Bool`.
* The `=>` signifies a class constraint.
* The example above shows `==` is a function that takes 2 things of the same Type and returns a Bool.
* It has a constraint on the class that is must be of Type `Eq`, meaning it must implement `==` and `/=`
* `Ord` - types that can be ordered e.g.
  * `:t (>) -- returns (>) :: (Ord a) => a -> a -> Bool`
* `show` - effectively turns the parameter into a string e.g.
  * `show 1234567890 -- returns "1234567890"`
* `read` - takes the input as a string and returns a type
  * `read "[0,1,2,3]" ++ [4] -- returns [0,1,2,3,4]`
  * `:t read -- returns read :: (Read a) => String -> a`
  * In order for `read` to work the compiler needs to know what type to return which can be achieved through type annotations e.g.
  * `read "[1,2,3]" -- throws an exception`
  * `read "[1,2,3]" :: [Int] -- returns [1,2,3]`. The compilers knows an array of Ints is going to be returned
* `fromIntegral` is a useful function in turning Integral numbers into the more generic Num numbers. Checking out the type declaration
  * `:t fromIntegral -- returns fromIntegral :: (Num b, Integral a) => a -> b`
  * Multiple class constraints specified and comma separated in parentheses
  * Type declaration shows taking an Integral type and returning a Num type
  * An example of its usefulness is when needing to add something to the result of the length function
  * `length "listofcharacters" + 3.5 -- throws exception`
  * `fromIntegral(length "listofcharacters") + 3.5 -- returns 19.5`

