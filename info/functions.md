# Functions

## Function application

* Function application has the highest precendence.
* Function application happens with spaces and NOT with parenthesis.
* Functions taking 2 parameters can be called through the infix method, by using backticks (`) e.g.
  * `div 10 2 -- returns 5`
  * ``10 `div` 2 -- returns 5``

## succ -- returns the successive item e.g.

* `succ 'p' -- returns 'q'`
* `succ 1234 -- returns 1235`

## min -- returns the min value from the 2 supplied e.g.
* `min 9 1 -- returns 1`
* `min 'd' 'c' -- returns 'c'`

## max -- returns the max value from the 2 supplied e.g.
* `max 9 1 -- returns 9`
* `max 'd' 'c' -- returns 'd'`


## Function definitions

* `doubleMe a = a*2`
* `doubleUs x y = x*2 + y*2`
* `doubleUs2 x y = doubleMe x + doubleMe y`


## if function

* The else in an if statement is mandatory
* `if then else` is an expression. An expression is something returns something, this is why the else is mandatory
* if statements can be written on one line or over several e.g.

```
doubleSmallNumber x = if x > 100 then x else x*2
doubleSmallNumber x = if x > 100
						then x
						else x*2
```


## Function naming conventions

* Using an apostrophe at the end of the name of a function is usually used to denote a strict function or a slightly modified version of another function of the same name e.g.
  * `doubleMe x = x * 2`
  * `doubleMe' x = (x * 2) + 1`
* A strict function is one that is not lazy
* An apostrophe is acceptable within the name of the function
* Function names must not begin with a capital letter
* A parameterless function is normally called a definition or a name. Names and functions can not be changed once defined e.g.
  * `sayMyName = "What is your name? This is your name!"`
