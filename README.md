# Haskell Learnings

## What is Haskell?

Haskell is a purely functional, lazy, statically typed programming language.

* Purely Functional
  * functions are values
  * Values never change
* Lazy
* Statically Typed


## Why should I learn Haskell?

* Add another language to your capabilities
* Challenging
* Has been born from a different background. One of Mathematics.
  * Developed over a long period of time where very few shortcuts have been taken.
  * Things have been done 'properly'.
  * Due to it being a developed in a research scenario it is at the cutting edge of computing (in terms of parallelisation, etc.) 
* Wide range of libraries
* Performance
* Proved in industry


## Tools

* The Glasglow Haskell Compiler or [GHC](http://en.wikipedia.org/wiki/Glasgow_Haskell_Compiler)
* Haskell platform [download](http://www.haskell.org/platform/)
* GHCi - REPL (Read, Evaluate, Print, Loop)


## Functions

* No commas, no parentheses
* Defining a function with a single parameter called 'square': `square x = x * x`
* Function with multiple parameters: `multiply a b = a * b`
* Function composed of another function with multiple parameters: `multiMax a b c = (max a b) * c`

### Pure functions

* Functions that always return the same result i.e. atomic
* Do not rely on any external state beyond the provided arguments

### Functions as values

* Functions can be used just like values

### Map

* Applies a function to every element in a list

### Filter

* Applies a boolean test to every element in a list, creating a new list with the elements passing the test

### Fold

* 2 versions - foldl and foldr

### Zip

* 'zips' 2 lists
* e.g. zip [1,2,3][4,5,6] -- returns [(1,4),(2,5),(3,6)]


## No loops

* If you think a loop is required - it isn't. Some recursion will do the job.


## Lists

* Must be homogeneous i.e. of the same type
* Colon ':' is called the 'cons' operator. It is used to construct lists along with square brackets

* x = [1,2,3]
* empty = []
* y = 0 : x -- [0,1,2,3]
* x' = 1 : (2 : (3 : []))
* x'' = 1 : 2 : : 3 : []

### List functions

* head [1,2,3] -- returns 1
* tail [1,2,3] -- returns [2,3]
* head (tail [1,2,3]) -- returns 2
* null [] -- returns True
* null [1] -- returns False


## Strings

* Just a list of characters e.g. "hello" == 'h' : 'e' : 'l' : 'l' : 'o' : []


## Operators

* Concatenation operator `++`.
* When concatenating a new variable is created.
* Are functions e.g. +, *, :, ++
* To use an operator as a function add parentheses e.g. (+), (*), (:), (++)
* (*) 7 9 -- returns 63 i.e. 7 * 9
* (+)((*) 7 9) 7 -- returns 70 i.e. (7*9) + 7
* Functions can be turned into operators with the back tick '`' e.g. mod 9 3 can be rewritten to 9 `mod` 3

## Tuples

* Can consist of multiple types
* Uses parentheses
* Fixed length
* Beyond a small handful of items in a Tuple it is best to use a custom data type

### Tuple functions

* fst (1, "hello") -- returns the first element i.e. 1
* snd (1, "hello") -- returns the second element i.e. "hello"


## Pattern Matching

* fst' (a,b) = a
* snd' (a,b) = b
* null' [] = True
* null' (x : xs) = False

Not a great thing to do 
* head' (x : xs) = x
* head' [] = ERROR -- really a bad thing to let happen


## Guards

* otherwise - a boolean expression that is always evaluated as true
**Need to put some examples in here**


## Case Expressions

**Need to put some examples in here**


## Let and Where Bindings


## Whitespace is significant

* Never use tabs
* Indenting is important, parameters 


## Lazy Function Evaluation

* Only evaluating what is required. This allows the ability to create infinite lists.


## Function composition

* Functions can be composed together through the use of the dot (.) operator


## Function application

* Applies a function to a value
* Uses the $ sign


## Type System

* Statically typed
* Type inference so mostly do not need to specify the type explictly unless it is useful for the legibility of the code
* Compile time type checking
* Using GHCi :t will tell you the type e.g. :t "string" -- returns [Char]
* Using :t will tell you the signature of the function e.g. :t lines -- returns string -> [string] which denotes a function that takes a string and returns a list of strings
* So a -> b is a function taking type a and returning type b
* Types can be annotated with their type but this is normally something to be done sparingly. If lots of type annotation is happening something it probably wrong
* Type variables start with a lowercase letter whereas Types start with an uppercase letter
* A double arrow (=>) is a constraint e.g. :t sum -- returns Num a => [a] -> a meaning with the constraint that a is Num Type Num is function that takes a Num Type and returns a value of the same Type as a
* There can be multiple Type Class Constraints which are represented by adding them in brackets

### Type Synonyms

* Improve readability e.g.
  * type String = [Char]
  * type Point = (Double, Double)
* The 'newtype' keyword allows a 'new' type of an existing type to be created e.g. rather than have an id as just an Int it could be made to be a ThingId type e.g. `newtype ThingId = MakeThingId Int` This is creating a new type `ThingId` and specifiying a constructor taking an int `MakeThingId Int`
* Typically the newtype constructor will be named the same as the new type e.g. `newtype ThingId = ThingId int`
* newtype can only have a single parameter

### Records

* Are a little bit like classes in OO languages but aren't really
* Are not extensible i.e. no inheritance possible
* Fields names can not be the same from one record to another e.g. if one record has a field 'identifier' no other record can have a field with the name 'identifier'

### Algebraic Data Type

* Bread and butter of Haskell
* e.g. `data Thing = MakeThing ThingId String Int`
* Can have multiple constructors
* Facilitates enum like data structures e.g. `data DayOfWeek = Monday | Tuesday | Wednesday | Thursday | Friday | Saturday | Sunday`


## Type Classes

* Type Class Instances
